# SocketQuiz

## Getting started

To run the servers, you first need to add your local variables to the .env.local files (in backend/.env.local and forntend/.env.local).

Follow the .env.template files on each folder, **be careful that it matches your compose.yml configuration**.

Once set up, run the following command:

```bash
docker-compose up --build -d
```

## Roles

- Yassine Bekioui (@twiney94): Dockerization, Quiz Room, Question Broadcast
- Kokou Daniel Amoudokpo (@danielamoudokpo): ...
- Fodékar Loutezamo (@fodekar): Scoring, Answer acknowledgement, Server stop watch