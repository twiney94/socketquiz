const express = require("express");
const { createServer } = require("node:http");
const { join } = require("node:path");
const { Server } = require("socket.io");
const dotenv = require("dotenv");
dotenv.config({ path: ".env.local" });
dotenv.config();

const app = express();

const server = createServer();
const io = new Server(server, {
  cors: {
    origin: "*",
    methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
    preflightContinue: false,
    optionsSuccessStatus: 204,
  },
});
const questionDuration = 30000;


let quizzes = [];

io.on('connection', (socket) => {
  console.log('Un utilisateur s\'est connecté');

  socket.on('createQuiz', (quiz) => {
    console.log('Un quiz a été créé');
    quizzes.push(quiz);
    console.log(quizzes);
  });

  socket.on('joinQuiz', (quizId) => {
    const questions = quizzes[quizId];
    if (questions) {
      socket.join(quizId);
      io.to(quizId).emit('quizStarted', questions);
    }
  });

  socket.on('joinRoom', (room) => {
    socket.join(room);
    console.log(`Un utilisateur a rejoint la salle ${room}`);
  });

  socket.on('startQuiz', (data) => {
    const { room, questions } = data;
    io.to(room).emit('quizStarted', questions);
  });

  socket.on('disconnect', () => {
    console.log('Un utilisateur s\'est déconnecté');
  });
});

app.post('/createQuiz', (req, res) => {
  const quizId = uuidv4();
  const { questions } = req.body;
  quizzes[quizId] = questions;
  res.json({ quizId, link: `http://localhost:${process.env.PORT || 3000}/quiz/${quizId}` });
});

server.listen(process.env.PORT || 3000, () => {
  console.log(`server running at http://localhost:${process.env.PORT || 3000}`);
});

let scores = {};
let userAnswers = {};
let questions = [
    {
        question: "What is 1 + 1?",
        answers: ["1", "2", "3", "4"],
        correctAnswer: "2",
    },
    {
        question: "What is 2 + 2?",
        answers: ["1", "2", "3", "4"],
        correctAnswer: "4",
    },
    {
        question: "What is 3 + 3?",
        answers: ["1", "2", "3", "6"],
        correctAnswer: "6",
    },
    {
        question: "What is 4 + 4?",
        answers: ["1", "2", "3", "8"],
        correctAnswer: "8",
    },
    {
        question: "What is 5 + 5?",
        answers: ["1", "2", "3", "10"],
        correctAnswer: "10",
    },
    {
        question: "What is 6 + 6?",
        answers: ["1", "2", "3", "12"],
        correctAnswer: "12",
    },
    {
        question: "What is 7 + 7?",
        answers: ["1", "2", "3", "14"],
        correctAnswer: "14",
    },
    {
        question: "What is 8 + 8?",
        answers: ["1", "2", "3", "16"],
        correctAnswer: "16",
    },
    {
        question: "What is 9 + 9?",
        answers: ["1", "2", "3", "18"],
        correctAnswer: "18",
    },
    {
        question: "What is 10 + 10?",
        answers: ["1", "2", "3", "20"],
        correctAnswer: "20",
    },
    {
        question: "What is 11 + 11?",
        answers: ["1", "2", "3", "22"],
        correctAnswer: "22",
    },
    {
        question: "What is 12 + 12?",
        answers: ["1", "2", "3", "24"],
        correctAnswer: "24",
    },
    {
        question: "What is 13 + 13?",
        answers: ["1", "2", "3", "26"],
        correctAnswer: "26",
    }
];

io.on("connection", (socket) => {
  console.log("un utilisateur s'est connecté");

  socket.on('submit_answer', (data) => {
    const { userId, questionId, answer, timeTaken } = data;
    const question = questions.find(q => q.id === questionId);
    if (question) {
        const score = scoring.calculateScore(question.correctAnswer, answer, timeTaken);
        if (!scores[userId]) {
            scores[userId] = 0;
        }
        scores[userId] += score;
        userAnswers[userId] = userAnswers[userId] || {};
        userAnswers[userId][questionId] = { answer, score };
        socket.emit('score_update', { userId, score: scores[userId] });
    }
  });
});

quizzes.forEach(quiz => {
  quiz.currentQuestionIndex = 0;
  quiz.questionTimers = [];
});

socket.on('startQuiz', (data) => {
  const { room, questions } = data;
  const quiz = quizzes.find(q => q.id === room);

  if (quiz) {
    startQuestionTimer(quiz, room);
  }
});

function startQuestionTimer(quiz, room) {
  if (quiz.currentQuestionIndex < quiz.questions.length) {
    io.to(room).emit('question', {
      question: quiz.questions[quiz.currentQuestionIndex],
      index: quiz.currentQuestionIndex,
    });

    quiz.questionTimers[quiz.currentQuestionIndex] = setTimeout(() => {
      io.to(room).emit('question_ended', { index: quiz.currentQuestionIndex });
      quiz.currentQuestionIndex++;
      startQuestionTimer(quiz, room);
    }, questionDuration);
  } else {
    io.to(room).emit('quiz_ended');
  }
}

socket.on('disconnect', () => {
  console.log('Un utilisateur s\'est déconnecté');
});

quizzes.forEach(quiz => {
  quiz.answers = {};
})

socket.on('submit_answer', (data) => {
  const { quizId, questionId, answer, userId } = data;
  const quiz = quizzes.find(q => q.id === quizId);

  if (quiz && quiz.currentQuestionIndex === questionId && !quiz.answers[questionId][userId]) {
    if (!quiz.answers[questionId]) {
      quiz.answers[questionId] = {};
    }
    if (!quiz.answers[questionId][answer]) {
      quiz.answers[questionId][answer] = [];
    }
    quiz.answers[questionId][answer].push(userId);

    io.to(quizId).emit('answer_update', {
      questionId: questionId,
      answer: answer,
      count: quiz.answers[questionId][answer].length
    });
  }
});

function startQuestionTimer(quiz, room) {
    if (quiz.currentQuestionIndex >= quiz.questions.length) {
        return;
    }
  quiz.questionTimers[quiz.currentQuestionIndex] = setTimeout(() => {
    io.to(room).emit('question_ended', { index: quiz.currentQuestionIndex });
    quiz.answers[quiz.currentQuestionIndex].locked = true;
    quiz.currentQuestionIndex++;
    startQuestionTimer(quiz, room);
  }, questionDuration);
}

module.exports = app;
