// backend/scoring.js
function calculateScore(correctAnswer, userAnswer, timeTaken) {
    const correct = correctAnswer === userAnswer;
    const timeScore = Math.max(0, 10 - timeTaken);
    const score = correct ? (1 + timeScore) : 0;
    return score;
}

module.exports = {
    calculateScore,
};
