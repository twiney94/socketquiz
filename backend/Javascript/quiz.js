socket.on('question', (data) => {
  displayQuestion(data.question);
});

socket.on('question_ended', (data) => {
    displayEnd();
});

socket.on('quiz_ended', () => {
    displayEnd();
});

socket.on('answer_update', (data) => {
  const { questionId, answer, count } = data;
  updateAnswerDisplay(questionId, answer, count);
});

function updateAnswerDisplay(questionId, answer, count) {
  const answerDisplay = document.getElementById(`answer-${questionId}-${answer}`);
  answerDisplay.textContent = count;
}

function updateAnswerDisplay(questionId, answer, count) {
  const answerElement = document.querySelector(`#answer_${answer}_count`);
  if (answerElement) {
    answerElement.textContent = `Réponses pour l'option ${answer}: ${count}`;
  }
}
