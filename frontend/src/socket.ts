import { reactive } from "vue";
import { io, Socket } from "socket.io-client";

interface State {
  connected: boolean;
  fooEvents: any[];
  barEvents: any[];
}

export const state: State = reactive({
  connected: false,
  fooEvents: [],
  barEvents: [],
});

export const socket: Socket = io(`http://localhost:${import.meta.env.VITE_BACKEND_PORT || 3000}`);
socket.on("connect", () => {
    state.connected = true;
});

socket.on("disconnect", () => {
  state.connected = false;
});

socket.on("foo", (...args: any[]) => {
  state.fooEvents.push(args);
});

socket.on("bar", (...args: any[]) => {
  state.barEvents.push(args);
});

export default socket;